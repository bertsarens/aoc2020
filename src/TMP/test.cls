Class TMP.Test
{

Property temp As %String;

ClassMethod test() As %Status
{
    set result = $$$OK
    w !,"hello world"
    return result
}

ClassMethod Day01()
{
    Set stream=##class(%Stream.FileCharacter).%New()
    Set sc=stream.LinkToFile("/aoc/day01.txt")
    While 'stream.AtEnd {
        s newValue=stream.ReadLine()
        s value=$ORDER(list(""))
        while value'="" {
            if (value+newValue)=2020 {
                w !,value*newValue
            }
            s value=$ORDER(list(value))
        }
        s list(newValue)=""
    ; Process the chunk here
    }

    s value1=$ORDER(list(""))
    while value1'="" {
        s value2=$ORDER(list(""))
        while value2'="" {
            if (value2+value1)<2020 {
                s value3=$ORDER(list(""))
                while value3'="" {
                    if (value1+value2+value3)=2020 {
                        w !,value1*value2*value3
                    }
                    s value3=$ORDER(list(value3))
                }
            }
            s value2=$ORDER(list(value2))
        }
        s value1=$ORDER(list(value1))
    }
}

ClassMethod Day02()
{
    s valid=0
    s valid2=0
    Set stream=##class(%Stream.FileCharacter).%New()
    Set sc=stream.LinkToFile("/aoc/day02.txt")
    While 'stream.AtEnd {
        s value=stream.ReadLine()
        w !,value
        // 3-7 f: frfshbfn
        s $LB(part1,part2,part3)=$LFS(value," ")
        s min=$PIECE(part1,"-")
        s max=$PIECE(part1,"-",2)
        s letter=$PIECE(part2,":")
        s word=part3
        s count =$LENGTH(word,letter)-1
        if (count>=min) && (count<=max) {
            s valid=valid+1
        }
        s count=0
        if $EXTRACT(word,min)=letter {
            s count=count+1
        }
        if $EXTRACT(word,max)=letter {
            s count=count+1
        }
        if count=1 {
            s valid2=valid2+1
        }
    }
    w !,valid
    w !,valid2
}

ClassMethod INC()
{
    s a=$zh
    s count=0
    for i=1:1:1000000 {
        s count=count+1
    }
    w !,"count=count+1 => ",$zh-a
    s a=$zh
    s count=0
    for i=1:1:1000000 {
        //1
        s count=$SEQUENCE(count)
    }
    w !,"$SEQUENCE(count) => ",$zh-a
    s a=$zh
    s count=0
    for i=1:1:1000000 {
        s count=$INCREMENT(count)
    }
    w !,"s count=$INCREMENT(count) => ",$zh-a
}

}
