Class AOC.day21
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day21.txt"
    if Example=1 s file="/aoc/day21example.txt"
    if Example=2 s file="/aoc/day21example2.txt"
    Set sc=stream.LinkToFile(file)

    While 'stream.AtEnd {
        s line=stream.ReadLine()
        //w !,line
        s ingPart=$PIECE(line,"(")
        s alePart=$PIECE(line,"(contains",2)
        //w !,ingPart
        //w !,alePart

        for i=1:1:$LENGTH(ingPart," ") {
            if $PIECE(ingPart," ",i)'="" {
                s ing=$PIECE(ingPart," ",i)
                s appearence(ing)=$GET(appearence(ing))+1
                for j=1:1:$LENGTH(alePart,",") {
                    s ale=$PIECE($TR(alePart," )"),",",j)
                    //w !,ing,*9,ale
                    s canContain(ing,ale)=$GET(canContain(ing,ale))+1
                    s possibleFoods(ale,ing)=$GET(possibleFoods(ale,ing))+1
                }
            }
        }
        for j=1:1:$LENGTH(alePart,",") {
            s ale=$PIECE($TR(alePart," )"),",",j)
            s possibleFoods(ale)=$GET(possibleFoods(ale))+1
        }
    }
    //zw canContain
    //zw possibleFoods

    s ale=$ORDER(possibleFoods(""),1,total)
    while ale'="" {
        s ing=$ORDER(possibleFoods(ale,""),1,count)
        while ing'="" {
            if count<total {
                k possibleFoods(ale,ing)
            } else {
                k appearence(ing)
            }
            s ing=$ORDER(possibleFoods(ale,ing),1,count)
        }
        s ale=$ORDER(possibleFoods(ale),1,total)
    }
    s sum=0
    s ing=$ORDER(appearence(""),1,total)
    while ing'="" {
        s sum=sum+total
        s ing=$ORDER(appearence(ing),1,total)
    }
    w !,sum,!

    zw possibleFoods
    // loop over possibleFoods, if you find one with one options, add it to result and remove it from all possibleFoods
    s doneWork=1
    while doneWork {
        s doneWork=0
        s ale=$ORDER(possibleFoods(""))
        while ale'="" {
            s ing=$ORDER(possibleFoods(ale,""))
            s ing2=$ORDER(possibleFoods(ale,ing))
            if ((ing2="") && ('$DATA(done(ale)))){
                w !,"found ",ing," as sole in: ",ale
                s result(ale)=ing
                s done(ale)=""
                d ..RemoveIngFromOther(.possibleFoods,ing,ale)
                s doneWork=1
            }
            s ale=$ORDER(possibleFoods(ale))
        }
    }

    w !
    s ale=$ORDER(result(""),1,ing)
    while ale'="" {
        w ing,","
        s ale=$ORDER(result(ale),1,ing)
    }
}

ClassMethod RemoveIngFromOther(PossibleFoods, Ing, Ale)
{
    s ale=$ORDER(PossibleFoods(""),1,total)
    while ale'="" {
        if ale'=Ale {
            s ing=$ORDER(PossibleFoods(ale,""),1,count)
            while ing'="" {
                if ing=Ing {
                    k PossibleFoods(ale,ing)
                }
                s ing=$ORDER(PossibleFoods(ale,ing),1,count)
            }
        }
        s ale=$ORDER(PossibleFoods(ale),1,total)
    }
}

}
