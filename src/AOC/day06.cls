Class AOC.day06
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day06.txt"
    if Example s file="/aoc/day06example.txt"
    Set sc=stream.LinkToFile(file)
    s count=0


    //s valid("cid")=""
    s con=""
    s result=0
    s sum=0
    s sum2=0
    s people=0
    While 'stream.AtEnd {
        s line=stream.ReadLine()
        if line="" {
            // count
            s sum=sum+..CountYes(con)
            s sum2=sum2+..CountYes2(con,people)
            s people=0
            s con=""
        } else {
            s people=people+1
            s con=con_line
        }
    }
    s sum=sum+..CountYes(con)
    s sum2=sum2+..CountYes2(con,people)
    w !,sum
    w !,sum2
}

ClassMethod CountYes(Con)
{
    k list
    s list=0
    for i=1:1:$LENGTH(Con) {
        if '$DATA(list($EXTRACT(Con,i))) {
            s list($EXTRACT(Con,i))=""
            s list=list+1
        }
    }
    return list
}

ClassMethod CountYes2(Con, People)
{
    k list
    s list=0
    for i=1:1:$LENGTH(Con) { 
        s list($EXTRACT(Con,i))=$GET(list($EXTRACT(Con,i)),0)+1
        if list($EXTRACT(Con,i))=People s list=list+1

    }
    return list
}

}
