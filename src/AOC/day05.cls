Class AOC.day05
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day05.txt"
    if Example s file="/aoc/day05example.txt"
    Set sc=stream.LinkToFile(file)
    s count=0

    s result=0
    s maxSeatID=0



    While 'stream.AtEnd {
        s line=stream.ReadLine()
        s row= ..FindNumber($EXTRACT(line,1,7))
        s column=..FindNumber($EXTRACT(line,8,10))
        s seatID=row*8+column
        if seatID>maxSeatID {
            s maxSeatID=seatID
        }
        s seats(seatID) =""
    }

    for row=0:1:127 {
        for column=0:1:7 {
            if '$DATA(seats(row*8+column)) {
                w !,"not found:",*9,row,*9,column,*9,row*8+column
            }
        }
    }
    w !,maxSeatID
}

ClassMethod FindNumber(Value)
{
    s min=0
    s max=2**($LENGTH(Value))-1
    f i=1:1:$LENGTH(Value) {
        s letter=$EXTRACT(Value,i)
        if ((letter="F") || (letter="L")) {
            //lower half
            s max = max- ((max-min)\2)-1
        } else {
            s min= min+ ((max-min)\2) +1
        }
    }
    return min
}

}
