Class AOC.day24
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day24.txt"
    if Example=1 s file="/aoc/day24example.txt"
    if Example=2 s file="/aoc/day24example2.txt"
    Set sc=stream.LinkToFile(file)


    While 'stream.AtEnd {
        s line=stream.ReadLine()
        s x=0
        s y=0
        s z=0

        for i=1:1:$LENGTH(line) {
            s char=$EXTRACT(line,i)
            if ((char="s") || (char="n")) {
                s i=i+1
                s char=char_$EXTRACT(line,i)
            }
            if char="e" {
                s x=x+1
                s y=y-1
            } elseif char="w" {
                s x=x-1
                s y=y+1
            } elseif char="se" {
                s z=z+1
                s y=y-1
            } elseif char="sw" {
                s x=x-1
                s z=z+1
            } elseif char="ne" {
                s x=x+1
                s z=z-1
            } elseif char="nw" {
                s y=y+1
                s z=z-1
            } else {
                w !,"whaattt?"
            }
        }
        s id=z_","_y_","_x
        if '$DATA(grid(id)) {
            s grid(id)=1
        } else {
            k grid(id)
        }
    }
    w !,..CountGrid(.grid)
    // start flipping tiles 
    s rounds=100
    f round=1:1:rounds {
        // only tiles adjecent to black tiles can flip so first calculate all possible flipping tiles
        k newGrid
        d ..GetPossibleFlips(.grid)
        //w !,"--Day ",round-1,": ",..CountGrid(.grid)
        // go over all of them and see if they flip
        s id=$ORDER(grid(""),1,isBlack)
        while id'="" {
            s blackNeighbhours=..GetAllBlackNeighbours(id,.grid)
            //w !,isBlack,*9,blackNeighbhours
            if (isBlack && ((blackNeighbhours=0) || (blackNeighbhours>2))) {
                // do nothing
            } elseif isBlack {
                s newGrid(id)=1
            }
            if ('isBlack && (blackNeighbhours=2)) {
                s newGrid(id)=1
            }
            s id=$ORDER(grid(id),1,isBlack)
        }
        k grid
        m grid=newGrid
        //w !,"Day ",round,": ",..CountGrid(.grid)
    }
    w !,"Day ",round,": ",..CountGrid(.grid)
}

ClassMethod CountGrid(Grid)
{
    s count=0
    s id=$ORDER(Grid(""),1,isBlack)
    while id'="" {
        if isBlack s count=count+1
        s id=$ORDER(Grid(id),1,isBlack)
    }
    return count
}

ClassMethod GetAllBlackNeighbours(Id, Grid)
{
    s result=0
    s z=$PIECE(Id,",")
    s y=$PIECE(Id,",",2)
    s x=$PIECE(Id,",",3)

    // east
    s dx=x+1
    s dy=y-1
    s dz=z
    if ($DATA(Grid(dz_","_dy_","_dx)) && (Grid(dz_","_dy_","_dx)=1)) {
        s result=result+1
    }
    // west
    s dx=x-1
    s dy=y+1
    s dz=z
    if ($DATA(Grid(dz_","_dy_","_dx)) && (Grid(dz_","_dy_","_dx)=1)) {
        s result=result+1
    }
    // south east
    s dz=z+1
    s dy=y-1
    s dx=x
    if ($DATA(Grid(dz_","_dy_","_dx)) && (Grid(dz_","_dy_","_dx)=1)) {
        s result=result+1
    }
    // south west
    s dx=x-1
    s dz=z+1
    s dy=y
    if ($DATA(Grid(dz_","_dy_","_dx)) && (Grid(dz_","_dy_","_dx)=1)) {
        s result=result+1
    }
    // north east
    s dx=x+1
    s dz=z-1
    s dy=y
    if ($DATA(Grid(dz_","_dy_","_dx)) && (Grid(dz_","_dy_","_dx)=1)) {
        s result=result+1
    }
    // north west
    s dy=y+1
    s dz=z-1
    s dx=x
    if ($DATA(Grid(dz_","_dy_","_dx)) && (Grid(dz_","_dy_","_dx)=1)) {
        s result=result+1
    }


    return result
}

ClassMethod GetPossibleFlips(Grid)
{
    m newGrid=Grid
    s id=$ORDER(Grid(""))
    while id'="" {
        s z=$PIECE(id,",")
        s y=$PIECE(id,",",2)
        s x=$PIECE(id,",",3)

        // east
        s dx=x+1
        s dy=y-1
        s dz=z
        if '$DATA(newGrid(dz_","_dy_","_dx)) s newGrid(dz_","_dy_","_dx)=0
        // west
        s dx=x-1
        s dy=y+1
        s dz=z
        if '$DATA(newGrid(dz_","_dy_","_dx)) s newGrid(dz_","_dy_","_dx)=0
        // south east
        s dz=z+1
        s dy=y-1
        s dx=x
        if '$DATA(newGrid(dz_","_dy_","_dx)) s newGrid(dz_","_dy_","_dx)=0
         // south west
        s dx=x-1
        s dz=z+1
        s dy=y
        if '$DATA(newGrid(dz_","_dy_","_dx)) s newGrid(dz_","_dy_","_dx)=0
         // north east
        s dx=x+1
        s dz=z-1
        s dy=y
        if '$DATA(newGrid(dz_","_dy_","_dx)) s newGrid(dz_","_dy_","_dx)=0
         // north west
        s dy=y+1
        s dz=z-1
        s dx=x
        if '$DATA(newGrid(dz_","_dy_","_dx)) s newGrid(dz_","_dy_","_dx)=0

        s id=$ORDER(Grid(id))
    }
    m Grid=newGrid
}

}
