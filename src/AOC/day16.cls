Class AOC.day16
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day16.txt"
    if Example=1 s file="/aoc/day16example.txt"
    if Example=2 s file="/aoc/day16example2.txt"
    Set sc=stream.LinkToFile(file)

    s turn=1
    s lastNumber=""
    s part=0
    s myTicket=""
    s tickets=0
    s fields=0
    While 'stream.AtEnd {
        s line=stream.ReadLine()
        s firstPart=$PIECE(line,":")
        //w !,firstPart
        if firstPart="your ticket" {
            s part=part+1
        } elseif firstPart="nearby tickets" {
            s part=part+1
        } elseif line'="" {
            if part=0 {
                s secondPart=$PIECE(line,":",2)
                s min1=$PIECE($PIECE(secondPart," ",2),"-",1)
                s max1=$PIECE($PIECE(secondPart," ",2),"-",2)
                s min2=$PIECE($PIECE(secondPart," ",4),"-",1)
                s max2=$PIECE($PIECE(secondPart," ",4),"-",2)
                s fields(firstPart,"min1")=min1
                s fields(firstPart,"max1")=max1
                s fields(firstPart,"min2")=min2
                s fields(firstPart,"max2")=max2
            } elseif part=1 {
                s list=$LFS(line,",")
                s myTicket=list
            } elseif part=2 {
                s list=$LFS(line,",")
                s tickets=tickets+1
                s tickets(tickets)=list
            }
        }
    }
    s sum=0
    s ticket=$ORDER(tickets(""),1,list)
    while ticket'="" {
        for i=1:1:$LL(list) {
            // test $LI(list,i)
            if '..IsValid($LI(list,i),.fields) {
                s sum=sum+$LI(list,i)
                k tickets(ticket)
            }

        }
        s ticket=$ORDER(tickets(ticket),1,list)
    }
    w !,sum,!

    // put all possible options on all possible fields
    s field=$ORDER(fields(""))
    while field'="" {
        for i=1:1:$LL(myTicket) {
            s fields(field,"options")=$GET(fields(field,"options"))+1
            s fields(field,"options",i)=""
            s todo(i)=""
        }
        s field=$ORDER(fields(field))
    }
    // go over tickets and cross options
    // if one option remains, dont do that position anymore
    s ticket=$ORDER(tickets(""),1,list)
    while ticket'="" {
        for i=1:1:$LL(myTicket) {
            if $DATA(todo(i)) {
                s field=$ORDER(fields(""))
                while field'="" {
                    if fields(field,"options")>1 {
                        s ok=0
                       if (($LI(list,i)<=fields(field,"max1")) && ($LI(list,i)>=fields(field,"min1"))) {
                            s ok=1
                        }
                        if (($LI(list,i)<=fields(field,"max2")) && ($LI(list,i)>=fields(field,"min2"))) {
                            s ok=1
                        }
                        if 'ok {
                            s fields(field,"options")=fields(field,"options")-1
                            k fields(field,"options",i)
                            if fields(field,"options")=1 {
                                k todo(i)
                            }
                        }
                    }
                    s field=$ORDER(fields(field))
                }                
            }
        }
        s ticket=$ORDER(tickets(ticket),1,list)
    }

    // nog eens door options lopen en degene uitzetten die maar 1 optie hebben.
    s doneSomething=1
    while (doneSomething) {
        s doneSomething=0
        s field=$ORDER(fields(""))
        while field'="" {
            if fields(field,"options")=1 {
                s option=$ORDER(fields(field,"options",""))
                //kill it in other fields
                s field2=$ORDER(fields(""))
                while field2'="" {
                    if field2'=field {
                        if $DATA(fields(field2,"options",option)) {
                            k fields(field2,"options",option)
                            s fields(field2,"options")=fields(field2,"options")-1
                            s doneSomething=1
                        }
                    }
                    s field2=$ORDER(fields(field2))
                }

            }
            s field=$ORDER(fields(field))
        }
    }
    s prod=1
    s field=$ORDER(fields("departure"))
    while ((field'="") && (field["departure")) {
        w !,field
        s option=$ORDER(fields(field,"options",""))
        w *9,option,*9,$LI(myTicket,option)
        s prod=prod*$LI(myTicket,option)
        s field=$ORDER(fields(field))
    }
    w !,prod
}

ClassMethod IsValid(Number, Fields)
{
    s result=0
    s field=$ORDER(Fields(""))
    while field'="" {
        if ((Number<=Fields(field,"max1")) && (Number>=Fields(field,"min1"))) {
            return 1
        }
        if ((Number<=Fields(field,"max2")) && (Number>=Fields(field,"min2"))) {
            return 1
        }
        s field=$ORDER(Fields(field))
    }
    return result
}

}
