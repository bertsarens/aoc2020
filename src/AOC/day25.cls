Class AOC.day25
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day25.txt"
    if Example=1 s file="/aoc/day25example.txt"
    if Example=2 s file="/aoc/day25example2.txt"
    Set sc=stream.LinkToFile(file)


    While 'stream.AtEnd {
        s line=stream.ReadLine()
        s cardPubKey=line
        s line=stream.ReadLine()
        s doorPubKey=line 
    }
    s value=1
    s subjectNumber=7
    s cardSecretLoopSize=0
    while value'=cardPubKey {
        s value=..DoLoop(value,subjectNumber)
        s cardSecretLoopSize=cardSecretLoopSize+1
    }
    w !,value,*9,cardSecretLoopSize
    s value=1
    s doorSecretLoopSize=0  
    while value'=doorPubKey {
        s value=..DoLoop(value,subjectNumber)
        s doorSecretLoopSize=doorSecretLoopSize+1
    }
    w !,value,*9,doorSecretLoopSize

    s value=1
    for i=1:1:doorSecretLoopSize {
        s value=..DoLoop(value,cardPubKey)
    }
    w !,value
}

ClassMethod DoLoop(Value, SubjectNumber)
{
    s Value=Value*SubjectNumber
    s Value=Value#20201227
    return Value
}

}
