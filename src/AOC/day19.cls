Class AOC.day19
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day19.txt"
    if Example=1 s file="/aoc/day19example.txt"
    if Example=2 s file="/aoc/day19example2.txt"
    Set sc=stream.LinkToFile(file)

    s messages=0
    While 'stream.AtEnd {
        s line=stream.ReadLine()
        if $LENGTH(line,":")=2 {
            s rule=$PIECE(line,":",2)
            if rule["""" {
                s rule=$TR(rule,""" ")
                s solved($PIECE(line,":"),rule)=""
            }
            s rules($PIECE(line,":"))=rule
            s rules($PIECE(line,":"),"UNSOLVED")=0
            // split in parts
            if $LENGTH(rule," ")>1 {
                s uns=0
                for i=1:1:$LENGTH(rule," ") {
                    s piece=$PIECE(rule," ",i)
                    if ((piece'="") && (piece'="|")) {
                        if '$DATA(rules($PIECE(line,":"),"UNSOLVED",piece)) {
                            s uns=uns+1
                            s rules($PIECE(line,":"),"UNSOLVED")=uns
                            s rules($PIECE(line,":"),"UNSOLVED",piece)=""
                        }
                    }
                }
            }
        } else {
            if line'="" {
                s messages=messages+1
                s messages(messages)=line
            }
        }
    }

    d ..SolveRules(.solved,.rules)
    s result=0
    s id=$ORDER(messages(""),1,message)
    while id'="" {
        //w !,message
        if $DATA(solved(0,message)) {
            s result=result+1
        }
        s id=$ORDER(messages(id),1,message)
    }
    w !,result
    //zw solved
}

ClassMethod Run2(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day19.txt"
    if Example=1 s file="/aoc/day19example.txt"
    if Example=2 s file="/aoc/day19example2.txt"
    Set sc=stream.LinkToFile(file)

    s maxMessageLength=0
    s messages=0
    While 'stream.AtEnd {
        s line=stream.ReadLine()
        if $LENGTH(line,":")=2 {
            s rule=$PIECE(line,":",2)
            if rule["""" {
                s rule=$TR(rule,""" ")
                s solved($PIECE(line,":"),rule)=""
            }
            s rules($PIECE(line,":"))=rule
            s rules($PIECE(line,":"),"UNSOLVED")=0
            // split in parts
            if $LENGTH(rule," ")>1 {
                s uns=0
                for i=1:1:$LENGTH(rule," ") {
                    s piece=$PIECE(rule," ",i)
                    if ((piece'="") && (piece'="|")) {
                        if '$DATA(rules($PIECE(line,":"),"UNSOLVED",piece)) {
                            s uns=uns+1
                            s rules($PIECE(line,":"),"UNSOLVED")=uns
                            s rules($PIECE(line,":"),"UNSOLVED",piece)=""
                        }
                    }
                }
            }
        } else {
            if line'="" {
                s messages=messages+1
                s messages(messages)=line
                if $LENGTH(line)>maxMessageLength s maxMessageLength=$LENGTH(line)
            }
        }
    }

    k rules(0)
    k rules(8)
    k rules(11)

    d ..SolveRules(.solved,.rules)
    /* idea for 2nd solve. dont solve 8 / 11 and 0
    basicly the new check must be
    8: 42 | 42 8
    11: 42 31 | 42 11 31
    0: 8 11
    every valid combination is:
    42 (or a multiple of it) followed by 31 (or a multiple of it)
    // There must be at least two 42 and one 31
    // for each 31 at the end we need at least one 42 + at least one 42 at the start extra
    // kijk hoeveel er max vooraan staan 42, kijk hoeveel er max achteraan staan // Zeker zijn dat er niets tussen zit offcourse.
    // Also der mogen veel 42 zijn en maar 1 31

    maybe create a combination of messages that has more length than longest message and then test those
    */
    //zw solved(42)
    //zw solved(31)

    s solveLength=$LENGTH($ORDER(solved(42,"")))

    w !,"max message length: ",maxMessageLength
    w !,"solve length: ",solveLength

    s result=0
    s id=$ORDER(messages(""),1,message)
    while id'="" {
        //w !!,message,*9,$LENGTH(message)
        if '($LENGTH(message)#solveLength) {
            s minSolve=$LENGTH(message)/solveLength
            s found42=0
            s notDone=1
            s i=1 
            while (notDone) && (i<=(minSolve)) {
                s start=(i-1)*solveLength+1
                s part=$EXTRACT(message,start,start+solveLength-1)
                //w part," "
                if $DATA(solved(42,part)) {
                    s found42=found42+1
                } else {
                    // stop search
                    s notDone=0
                }
                s i=i+1
            }
            //w !,"found42: ",found42
            s found31=0
            s notDone=1
            s i=minSolve
            while (notDone) && (i>=1) {
                s start=(i-1)*solveLength+1
                s part=$EXTRACT(message,start,start+solveLength-1)
                //w part," "
                if $DATA(solved(31,part)) {
                    s found31=found31+1
                } else {
                    // stop search
                    s notDone=0
                }
                s i=i-1
            }
            //w !,"found31: ",found31
            
            // kijk hoeveel er max vooraan staan 42, kijk hoeveel er max achteraan staan // Zeker zijn dat er niets tussen zit offcourse.
            if (found31+found42)>=(minSolve) {
                // There must be at least two 42 and one 31
                if ((found42>=2) && (found31>=1)) {
                    // for each 31 at the end we need at least one 42 + at least one 42 at the start extra
                    if (found31)<=(found42-1) {
                         s result=result+1
                    }
                    //w !,"ok"
                }
            }
        }
        s id=$ORDER(messages(id),1,message)
    }
    w !,result
    //zw solved
}

ClassMethod SolveRules(Solved, Rules)
{
    s isWorkDone=1
    while isWorkDone {
        s isWorkDone=0
        // go over solved rules, see if the exist unsolved somewhere else
        // solve them there
        s rule=$ORDER(Solved(""))
        while rule'="" {
            s ruleToSolve=$ORDER(Rules(""))
            while ruleToSolve'="" {
                if $DATA(Rules(ruleToSolve,"UNSOLVED",rule)) {
                    s Rules(ruleToSolve,"UNSOLVED")=$GET(Rules(ruleToSolve,"UNSOLVED"))-1
                    k Rules(ruleToSolve,"UNSOLVED",rule)
                }
                s ruleToSolve=$ORDER(Rules(ruleToSolve))
            }
            s rule=$ORDER(Solved(rule))
        }
        // at the end go over the rules that have no more unsolved and solve them
        s ruleToSolve=$ORDER(Rules(""))
        while ruleToSolve'="" {
            if (($GET(Rules(ruleToSolve,"UNSOLVED"))=0) && ('$DATA(Solved(ruleToSolve)))) {
                d ..SolveRule(.Solved,.Rules,ruleToSolve)
                if ruleToSolve'=0 {
                    s isWorkDone=1
                } else {
                    return 
                }
            }
            s ruleToSolve=$ORDER(Rules(ruleToSolve))
        }


        
    }
}

ClassMethod SolveRule(Solved, Rules, Rule)
{
    s toSolve=Rules(Rule)
    //w !,"solving ",Rule,*9,toSolve
    for i=1:1:$LENGTH(toSolve,"|") {
        s part=$PIECE(toSolve,"|",i)
        s solve1=$PIECE(part," ",2)
        s solve2=$PIECE(part," ",3)
        s solve3=$PIECE(part," ",4)
        //w !,"solving part ",i,*9,solve1,*9,solve2
        s sol=$ORDER(Solved(solve1,""))
        while sol'="" {
            if solve2'="" {
                s prev=$ORDER(Solved(solve2,""))
                while prev'="" {
                    if solve3'="" {
                        s next=$ORDER(Solved(solve3,""))
                        while next'="" {       
                            s Solved(Rule,sol_prev_next)=""
                            s next=$ORDER(Solved(solve3,next))
                        }
                    } else {
                        s Solved(Rule,sol_prev)=""
                    }
                    s prev=$ORDER(Solved(solve2,prev))
                }
            } else {
                s Solved(Rule,sol)=""
            }
            s sol=$ORDER(Solved(solve1,sol))
        }
    }
}

}
