Class AOC.day08
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day08.txt"
    if Example s file="/aoc/day08example.txt"
    Set sc=stream.LinkToFile(file)

    s instructions=0


    While 'stream.AtEnd {
        s line=stream.ReadLine()
        //w !,line
        s ins=$PIECE(line," ")
        s arg=$PIECE(line," ",2)
        s instructions(instructions,"INS")=ins
        s instructions(instructions,"ARG")=arg
        s instructions=instructions+1
    }

    s isFinished=0
    s lastInstruction=0
    while 'isFinished {
        w !," starting new loop"
        k newinstructions
        m newinstructions=instructions
        d ..GetNextPermutation(.newinstructions,.lastInstruction)
        s result= ..RunProgram(.newinstructions,.isFinished)
    }
    w !,"result:",*9,result
    w !,"Finished:",*9,isFinished
}

ClassMethod GetNextPermutation(Instructions, LastInstruction)
{
    s ins=$ORDER(Instructions(LastInstruction))
    s isFound=0
    while 'isFound {
        if Instructions(ins,"INS")="nop" {
            s Instructions(ins,"INS")="jmp"
            s LastInstruction=ins
            s isFound=1
        } elseif Instructions(ins,"INS")="jmp" {
            s Instructions(ins,"INS")="nop"
            s LastInstruction=ins
            s isFound=1
        }
        s ins=$ORDER(Instructions(ins))
    }
}

ClassMethod RunProgram(Instructions, IsFinished)
{
    s isDone=0
    s result=""
    s acc=0
    s ip=0
    s IsFinished=0

    while ('isDone) && (ip<Instructions){
        //w !,ip,*9,acc
        s dip=1
        if $DATA(used(ip)) {
            s isDone=1
            s result=acc
        }
        s used(ip)=""
        s ins=Instructions(ip,"INS")
        s arg=Instructions(ip,"ARG")
        //w *9,ins,*9,arg
        if ins="jmp" {
            s dip=arg
        }
        if ins="acc" {
            s acc=acc+arg
        }
        s ip=ip+dip
    }
    if 'isDone s IsFinished=1
    if result="" s result=acc
    return result
}

}
