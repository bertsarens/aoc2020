Class AOC.day17
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day17.txt"
    if Example=1 s file="/aoc/day17example.txt"
    Set sc=stream.LinkToFile(file)
    s z=0
    s y=1
    s y=1
    s turns=6
    While 'stream.AtEnd {
        s line=stream.ReadLine()
        for i=1:1:$LENGTH(line) {
            if $EXTRACT(line,i)="#" {  
                s coords(z,y,i)=$EXTRACT(line,i)
            }
        }
        s y=y+1
    }
    d ..MinMax(.coords,.minX,.maxX,.minY,.maxY,.minZ,.maxZ)
    w !,minX,*9,maxX,*9,minY,*9,maxY,*9,minZ,*9,maxZ
    w !,..CountActive(.coords)

    for i=1:1:turns {
        k newCoords
        for z=minZ-1:1:maxZ+1 {
            for y=minY-1:1:maxY+1 {
                for x=minX-1:1:maxX+1 {
                    //calc new coords
                    s value=$GET(coords(z,y,x),".")
                    s count=..CountBuren(x,y,z,.coords)
                    //w !,count
                    if ((z=-1)&&(y=3)&&(x=3)) {
                        //w !,"den count",*9,count
                    }
                    if value="#" {
                        if ((count=2) || (count=3)) {
                            //if ((z=-1)&&(y=3)&&(x=3)) w !,"setting it because before"
                            s newCoords(z,y,x)="#"
                        }
                    } else {
                        if count=3 {
                            //if ((z=-1)&&(y=3)&&(x=3)) w !,"setting it because after"
                            s newCoords(z,y,x)="#"
                        }
                    }
                }
            }
        }
        k coords
        m coords=newCoords
        d ..MinMax(.coords,.minX,.maxX,.minY,.maxY,.minZ,.maxZ)
        w !,i,*9,..CountActive(.coords)
    }
    //zw coords
}

ClassMethod Run2(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day17.txt"
    if Example=1 s file="/aoc/day17example.txt"
    Set sc=stream.LinkToFile(file)
    s z=0
    s y=1
    s w=0
    s turns=6
    While 'stream.AtEnd {
        s line=stream.ReadLine()
        for i=1:1:$LENGTH(line) {
            if $EXTRACT(line,i)="#" {  
                s coords(w,z,y,i)=$EXTRACT(line,i)
            }
        }
        s y=y+1
    }
    d ..MinMax2(.coords,.minX,.maxX,.minY,.maxY,.minZ,.maxZ,.minW,.maxW)
    w !,minX,*9,maxX,*9,minY,*9,maxY,*9,minZ,*9,maxZ,*9,minW,*9,maxW
    w !,..CountActive2(.coords)

    //zw coords

    for i=1:1:turns {
        k newCoords
        for w=minW-1:1:maxW+1 {
            for z=minZ-1:1:maxZ+1 {
                for y=minY-1:1:maxY+1 {
                    for x=minX-1:1:maxX+1 {
                        //calc new coords
                        s value=$GET(coords(w,z,y,x),".")
                        s count=..CountBuren2(x,y,z,w,.coords)
                        if value="#" {
                            if ((count=2) || (count=3)) {
                                s newCoords(w,z,y,x)="#"
                            }
                        } else {
                            if count=3 {
                                s newCoords(w,z,y,x)="#"
                            }
                        }
                    }
                }
            }
        }
        k coords
        m coords=newCoords
        d ..MinMax2(.coords,.minX,.maxX,.minY,.maxY,.minZ,.maxZ,.minW,.maxW)
        w !,i,*9,..CountActive2(.coords)
    }
    //zw coords
}

ClassMethod CountActive(Coords)
{
    s result=0
    s z=$ORDER(Coords(""))
    while z'="" {
        s y=$ORDER(Coords(z,""))
        while y'="" {
            s x=$ORDER(Coords(z,y,""))
            while x'="" {
                s result=result+1
                s x=$ORDER(Coords(z,y,x))
            }            
            s y=$ORDER(Coords(z,y))
        }
        s z=$ORDER(Coords(z))
    }
    return result
}

ClassMethod CountActive2(Coords)
{
    s result=0
    s w=$ORDER(Coords(""))
    while w'="" {
        s z=$ORDER(Coords(w,""))
        while z'="" {
            s y=$ORDER(Coords(w,z,""))
            while y'="" {
                s x=$ORDER(Coords(w,z,y,""))
                while x'="" {
                    s result=result+1
                    s x=$ORDER(Coords(w,z,y,x))
                }            
                s y=$ORDER(Coords(w,z,y))
            }
            s z=$ORDER(Coords(w,z))
        }
        s w=$ORDER(Coords(w))
    }
    return result
}

ClassMethod drawCoords(Coords)
{
    d ..MinMax(.Coords,.minX,.maxX,.minY,.maxY,.minZ,.maxZ)
    for z=minZ:1:maxZ {
        w !,"z level: ",z,!
        for y=minY:1:maxY {
            w !
            for x=minX:1:maxX {
                w $GET(Coords(z,y,x),".")
            }
        }
    }
}

ClassMethod MinMax(Coords, MinX, MaxX, MinY, MaxY, MinZ, MaxZ)
{
    s MinZ=$ORDER(Coords(""))
    s MaxZ=$ORDER(Coords(""),-1)
    s MinY=$ORDER(Coords(MinZ,""))
    s MaxY=MinY
    s MinX=$ORDER(Coords(MinZ,MinY,""))
    s MaxX=MinX
    s z=$ORDER(Coords(""))
    while z'="" {
        s y=$ORDER(Coords(z,""))
        while y'="" {
            if y<MinY s MinY=y
            if y>MaxY s MaxY=y
            s x=$ORDER(Coords(z,y,""))
            while x'="" {
                if x<MinX s MinX=x
                if x>MaxX s MaxX=x
                s x=$ORDER(Coords(z,y,x))
            }            
            s y=$ORDER(Coords(z,y))
        }
        s z=$ORDER(Coords(z))
    }
}

ClassMethod MinMax2(Coords, MinX, MaxX, MinY, MaxY, MinZ, MaxZ, MinW, MaxW)
{
    s MinW=$ORDER(Coords(""))
    s MaxW=$ORDER(Coords(""),-1)
    s MinZ=$ORDER(Coords(MinW,""))
    s MaxZ=MinZ
    s MinY=$ORDER(Coords(MinW,MinZ,""))
    s MaxY=MinY
    s MinX=$ORDER(Coords(MinW,MinZ,MinY,""))
    s MaxX=MinX
    s w=$ORDER(Coords(""))
    while w'="" {
        s z=$ORDER(Coords(w,""))
        while z'="" {
            if z<MinZ s MinZ=z
            if z>MaxZ s MaxZ=z            
            s y=$ORDER(Coords(w,z,""))
            while y'="" {
                if y<MinY s MinY=y
                if y>MaxY s MaxY=y
                s x=$ORDER(Coords(w,z,y,""))
                while x'="" {
                    if x<MinX s MinX=x
                    if x>MaxX s MaxX=x
                    s x=$ORDER(Coords(w,z,y,x))
                }            
                s y=$ORDER(Coords(w,z,y))
            }
            s z=$ORDER(Coords(w,z))
        }
        s w=$ORDER(Coords(w))
    }
}

ClassMethod CountBuren(X, Y, Z, Coords)
{
    s count=0
    for dz=-1:1:1 {
        for dy=-1:1:1 {
            for dx=-1:1:1 {
                if ((dz'=0) || (dy'=0) || (dx'=0)) {
                    if $GET(Coords(Z+dz,Y+dy,X+dx))="#" {
                        s count=count+1
                    }
                }
            }
        }
    }
    return count
}

ClassMethod CountBuren2(X, Y, Z, W, Coords)
{
    s count=0
    for dw=-1:1:1 {
        for dz=-1:1:1 {
            for dy=-1:1:1 {
                for dx=-1:1:1 {
                    if ((dw'=0) || (dz'=0) || (dy'=0) || (dx'=0)) {
                        if $GET(Coords(W+dw,Z+dz,Y+dy,X+dx))="#" {
                            s count=count+1
                        }
                    }
                }
            }
        }
    }
    return count
}

}
