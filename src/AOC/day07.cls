Class AOC.day07
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day07.txt"
    if Example s file="/aoc/day07example.txt"
    Set sc=stream.LinkToFile(file)

    /*
    structure
    bag,"parent"=name
    bag,"child",name=count
    */

    While 'stream.AtEnd {
        s line=stream.ReadLine()
        //w !,line
        s bagName=$PIECE(line," ",1,2)
        s bags(bagName)=""
        for i=1:1:$LENGTH(line,",") {
            s piece=$PIECE(line,",",i)
            s number=$PIECE(piece," ",$LENGTH(piece," ")-3)
            s subBagName=$PIECE(piece," ",$LENGTH(piece," ")-2,$LENGTH(piece," ")-1)
            if number'="contain" {
                s bags(bagName,"child",subBagName)=number
                s bags(subBagName,"parent",bagName)=number
            }

        }
    }
    d ..GetAllParents(.bags,"shiny gold",.result)
    zw result
    //zw bags
    w !,..CountAllChildren(.bags,"shiny gold")-1
}

ClassMethod GetAllParents(Bags, BagName, Result)
{

    s bag=$ORDER(Bags(BagName,"parent",""))
    while bag'="" {
        if '$DATA(Result(bag)) {
            s Result(bag)=""
            s Result=$GET(Result)+1
            d ..GetAllParents(.Bags,bag,.Result)
        }
        s bag=$ORDER(Bags(BagName,"parent",bag))
    }
}

ClassMethod CountAllChildren(Bags, BagName)
{
    if +Bags(BagName) return Bags(BagName)
    s result=1
    s bag=$ORDER(Bags(BagName,"child",""),1,number)
    while bag'="" {
        s result=result+(number*..CountAllChildren(.Bags,bag))
        s bag=$ORDER(Bags(BagName,"child",bag),1,number)
    }
    //w !,BagName,*9,result
    s Bags(BagName)=result
    return result
}

}
