Class AOC.day11
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day11.txt"
    if Example=1 s file="/aoc/day11example.txt"
    Set sc=stream.LinkToFile(file)

    s instructions=0
    s y=1
    While 'stream.AtEnd {
        s line=stream.ReadLine()
        f x=1:1:$LENGTH(line) {
            s room(y,x)=$EXTRACT(line,x)
        }
        s y=y+1
    }

    //d ..DrawRoom(.room)
    m origRoom=room
    s changed=1
    while changed {
        s changed=..DoStep(.room)
        //d ..DrawRoom(.room)
       
    }
    w !,..CountOcc(.room),!
    k room
    m room=origRoom
    s changed=1
    while changed {
        s changed=..DoStep2(.room)
        //d ..DrawRoom(.room) 
    }
    w !,..CountOcc(.room),!
}

ClassMethod DoStep2(Room)
{
    s changed=0
    s y=$ORDER(Room(""))
    while y'="" {
        s x=$ORDER(Room(y,""),1,data)
        while x'="" {
            s newRoom(y,x)=data
            if data="L" {
                if ..CountAdjecent2(.Room,x,y)=0 {
                    s newRoom(y,x)="#"
                    s changed=1
                }
            } elseif data="#" {
                if ..CountAdjecent2(.Room,x,y)>=5 {
                    s newRoom(y,x)="L"
                    s changed=1
                }
            }
            s x=$ORDER(Room(y,x),1,data)
        }
        s y=$ORDER(Room(y))
    }
    k Room
    m Room=newRoom
    return changed
}

ClassMethod DoStep(Room)
{
    s changed=0
    s y=$ORDER(Room(""))
    while y'="" {
        s x=$ORDER(Room(y,""),1,data)
        while x'="" {
            s newRoom(y,x)=data
            if data="L" {
                if ..CountAdjecent(.Room,x,y)=0 {
                    s newRoom(y,x)="#"
                    s changed=1
                }
            } elseif data="#" {
                if ..CountAdjecent(.Room,x,y)>=4 {
                    s newRoom(y,x)="L"
                    s changed=1
                }
            }
            s x=$ORDER(Room(y,x),1,data)
        }
        s y=$ORDER(Room(y))
    }
    k Room
    m Room=newRoom
    return changed
}

ClassMethod CountAdjecent(Room, SeatX, SeatY)
{
    s debug=0
    /*
    if ((SeatX=1) && (SeatY=2)) {
        s debug=1
    }*/
    s occ=0
    for y=-1:1:1 {
        for x=-1:1:1 {
            if debug=1 w !,SeatY+y,*9,SeatX+x
            if ((y'=0) || (x '=0)) {
                if $GET(Room(SeatY+y,SeatX+x))="#" {
                    s occ=occ+1
                    if debug w *9,$GET(Room(SeatY+y,SeatX+x)),*9,"counted"
                }

            }
        }
    }
    //w !,occ
    return occ
}

ClassMethod CountAdjecent2(Room, SeatX, SeatY)
{
    s debug=0
    /*
    if ((SeatX=1) && (SeatY=2)) {
        s debug=1
    }*/
    s occ=0
    for y=-1:1:1 {
        for x=-1:1:1 {
            if debug=1 w !,SeatY+y,*9,SeatX+x
            if ((y'=0) || (x '=0)) {
                // keep stepping till you find a non existing tile, an empty seat or occ seat
                s isDone=0
                s step=1
                while 'isDone {
                    s dx=x*step
                    s dy=y*step
                    s step=step+1
                    if $GET(Room(SeatY+dy,SeatX+dx))="#" {
                        s occ=occ+1
                        if debug w *9,$GET(Room(SeatY+dy,SeatX+dx)),*9,"counted"
                        s isDone=1
                    } elseif $GET(Room(SeatY+dy,SeatX+dx))="L" {
                        s isDone=1   
                    } elseif $GET(Room(SeatY+dy,SeatX+dx))="" {
                        s isDone=1
                    }
                }
            }
        }
    }
    //w !,occ
    return occ
}

ClassMethod DrawRoom(Room)
{
    w !
    s y=$ORDER(Room(""))
    while y'="" {
        w !
        s x=$ORDER(Room(y,""),1,data)
        while x'="" {
            w data
            s x=$ORDER(Room(y,x),1,data)
        }
        s y=$ORDER(Room(y))
    }
}

ClassMethod CountOcc(Room)
{
    s result=0
    s y=$ORDER(Room(""))
    while y'="" {
        s x=$ORDER(Room(y,""),1,data)
        while x'="" {
            if data="#" s result=result+1
            s x=$ORDER(Room(y,x),1,data)
        }
        s y=$ORDER(Room(y))
    }
    return result
}

}
