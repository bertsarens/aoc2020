Class AOC.day03
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day03.txt"
    if Example s file="/aoc/day03example.txt"
    Set sc=stream.LinkToFile(file)
    s y=0
    While 'stream.AtEnd {
        s line=stream.ReadLine()
        for i=1:1:$LENGTH(line) {
            s forest(y,i-1)=$EXTRACT(line,i)
        }
        s maxX=i-1
        s maxY=y
        s y=y+1
    }
    s incX=3
    s incY=1
    s a=..slopeCount(incX,incY,maxX,maxY,.forest)
    s incX=1
    s incY=1
    s b=..slopeCount(incX,incY,maxX,maxY,.forest)
    s incX=5
    s incY=1
    s c=..slopeCount(incX,incY,maxX,maxY,.forest)
    s incX=7
    s incY=1
    s d=..slopeCount(incX,incY,maxX,maxY,.forest)
    s incX=1
    s incY=2
    s e=..slopeCount(incX,incY,maxX,maxY,.forest)
    w !,a," ",b," ",c," ",d," ",e
    w !,a*b*c*d*e
}

ClassMethod slopeCount(IncX, IncY, MaxX, MaxY, ByRef Forest) As %Integer
{
    set result=0
       
    s curX=0
    s curY=0
    //gives 229 should be 228
    while curY<MaxY {
        s curY=curY+IncY
        s curX=curX+IncX

        s tmpX=curX#(MaxX+1)
        s tmpY=curY#(MaxY+1)
        if Forest(tmpY,tmpX)="#" {
            s result=result+1
        }
    }
    return result
}

}
