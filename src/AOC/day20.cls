Class AOC.day20
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day20.txt"
    if Example=1 s file="/aoc/day20example.txt"
    if Example=2 s file="/aoc/day20example2.txt"
    Set sc=stream.LinkToFile(file)

    s messages=0
    s y=0
    s count=0
    While 'stream.AtEnd {
        s line=stream.ReadLine()
        if line["Tile" {
            s tile=$EXTRACT($PIECE(line," ",2),1,*-1)
            //w !,tile
            s y=0
            s count=count+1
        } elseif line'="" {
            s y=y+1
            for i=1:1:$LENGTH(line) {
                s tiles(tile,y,i)=$EXTRACT(line,i)
            }
        }
    }
    // eerste idee, enkel de edge zijn belangrijk.
    // idee om alle mogelijke combinaties uit te rekenen en dan te kijken of ze kunnen?
    // hoeveel mogelijke rotate + flip combinaties zijn er  8 denk ik / rotate + flip op elke positie
    // misschien brute force maar eerst alle edges vergelijken om te zien welke geen buur kunnen zijn van een andere, en dan pas uitrekenen.
    // niet greedy zijn, dus niet op voorhand alles uitrekenen, maar gewoon kijken of het nog kan.
    // misschien eerst op basis van possible neighbhours alle mogelijke combinaties uitrekenen? Zouden dat er nog veel zijn?

    s tileID=$ORDER(tiles(""))
    while tileID'="" {
         d ..GetEdges(.tiles,tileID,.result)
         m edges(tileID)=result
        s tileID=$ORDER(tiles(tileID))
    }

    s tileID=$ORDER(tiles(""))
    while tileID'="" {
         d ..GetPossibleBuren(.edges,tileID,.result)
         m buren(tileID)=result
        s tileID=$ORDER(tiles(tileID))
    }
   
    s prod=1
    s tileID=$ORDER(tiles(""))
    while tileID'="" {
        if buren(tileID)=2 {
            s prod=prod*tileID
        }
        s counts(buren(tileID))=$GET(counts(buren(tileID)))+1
        s reverseBuren(buren(tileID),tileID)=""
        s tileID=$ORDER(tiles(tileID))
    }
    w !,count,*9,$ZSQR(count)
    s edgeSize=$ZSQR(count)
    w !,prod,!
    zw counts

    d ..FindSolution(.tiles,.buren,.reverseBuren,.solution,edgeSize)
    //zw solution
    // orientation of the tiles in the solution is not correct.
    d ..RotateSolution(.tiles,.solution,.buren,.edges)
    // Create new image without the borders
    d ..CreateImage(.tiles,.solution,.image,10)
    // get possible 8 combinations and see if one contains sea monsters
    
    //
    m images(1)=image
    d ..GetAllPossibleTransformations(.images,1,.transformations)
    s id=$ORDER(transformations(""))
    while id'="" {
        m temp=transformations(id)
        //d ..DrawTile(.temp)
        s result=..FindWhale(.temp)
        if result>0 {
            w !!,id
            w !,"result: ",result
            s count=..CountWaves(.temp)
            w !,"count: ",count
            w !,"answer: ",count-(result*15)

        }
        s id=$ORDER(transformations(id))
    }
    
    /*
    s tile(1,1)=1
    s tile(1,2)=2
    s tile(1,3)=3
    s tile(2,1)=4
    s tile(2,2)=5
    s tile(2,3)=6
    s tile(3,1)=7
    s tile(3,2)=8
    s tile(3,3)=9
    d ..DrawTile(.tile)
    d ..Rotate(.tile)
    d ..FlipRows(.tile)
    d ..DrawTile(.tile)
    */

    //zw buren
}

ClassMethod CountWaves(Image)
{
    s result=0
    s y=$ORDER(Image(""))
    while y'="" {
        s x=$ORDER(Image(y,""),1,data)
        while x'="" {
            if data="#" s result=result+1
            s x=$ORDER(Image(y,x),1,data)
        }
        s y=$ORDER(Image(y))
    }
    return result
}

ClassMethod FindWhale(Image)
{
    s result=0
    s y=$ORDER(Image(""))
    while y'="" {
        s x=$ORDER(Image(y,""))
        while x'="" {
            s isWhale=1
            s Sy=y 
            s Sx=x 
            /*
            .#.#...#.###...#.##.O#..
            #.O.##.OO#.#.OO.##.OOO##
            ..#O.#O#.O##O..O.#O##.##
            */
            if $GET(Image(Sy,Sx),".")="." s isWhale=0
            if $GET(Image(Sy+1,Sx+1),".")="." s isWhale=0
            if $GET(Image(Sy+1,Sx+4),".")="." s isWhale=0
            if $GET(Image(Sy,Sx+5),".")="." s isWhale=0
            if $GET(Image(Sy,Sx+6),".")="." s isWhale=0
            if $GET(Image(Sy+1,Sx+7),".")="." s isWhale=0
            if $GET(Image(Sy+1,Sx+10),".")="." s isWhale=0
            if $GET(Image(Sy,Sx+11),".")="." s isWhale=0
            if $GET(Image(Sy,Sx+12),".")="." s isWhale=0
            if $GET(Image(Sy+1,Sx+13),".")="." s isWhale=0
            if $GET(Image(Sy+1,Sx+16),".")="." s isWhale=0
            if $GET(Image(Sy,Sx+17),".")="." s isWhale=0
            if $GET(Image(Sy,Sx+18),".")="." s isWhale=0
            if $GET(Image(Sy,Sx+19),".")="." s isWhale=0
            if $GET(Image(Sy-1,Sx+18),".")="." s isWhale=0

            if isWhale {
                s result=result+1
            }
            s x=$ORDER(Image(y,x))
        }
        s y=$ORDER(Image(y))
    }
    return result
}

ClassMethod GetAllPossibleTransformations(Tiles, TileID, Transformations)
{
    k Transformations
    m Transformations(1)=Tiles(TileID)
    m temp=Tiles(TileID)
    d ..FlipRows(.temp)
    m Transformations(2)=temp
    m temp=Transformations(1)
    d ..Rotate(.temp)
    m Transformations(3)=temp
    d ..FlipRows(.temp)
    m Transformations(4)=temp
    m temp=Transformations(3)
    d ..Rotate(.temp)
    m Transformations(5)=temp
    d ..FlipRows(.temp)
    m Transformations(6)=temp
    m temp=Transformations(5)
    d ..Rotate(.temp)
    m Transformations(7)=temp
    d ..FlipRows(.temp)
    m Transformations(8)=temp
}

ClassMethod RotateSolution(Tiles, Solution, Buren, Edges)
{
    // basicly
    // ga over de tiles in de solution 1 voor 1
    // rotate en flip tile tot dat hij de edge die match met de buren heeft waar die moet zijn
    s y=$ORDER(Solution(""))
    while y'="" {
        s x=$ORDER(Solution(y,""),1,tileID)
        while x'="" {
            // vindt gemeenschappelijke edge met buren
            k todo
            s rechterbuur=$GET(Solution(y,x+1))
            if rechterbuur'="" s todo(1)=Solution(y,x+1)
            s linkerbuur=$GET(Solution(y,x-1))
            if linkerbuur'="" s todo(2)=Solution(y,x-1)
            s bovenbuur=$GET(Solution(y-1,x))
            if bovenbuur'="" s todo(3)=Solution(y-1,x)
            s onderbuur=$GET(Solution(y+1,x))
            if onderbuur'="" s todo(4)=Solution(y+1,x)
            //zw todo
            //w ..GetEdge(.Tiles,tileID,1),!
            d ..GetAllPossibleTransformations(.Tiles,tileID,.transformations)
            for i=1:1:4 {
                if $DATA(todo(i)) {
                    s trans=$ORDER(transformations(""))
                    while trans'="" {
                        //w !,"trans: ",trans
                        m temp(tileID)=transformations(trans)
                        s edge=..GetEdge(.temp,tileID,i)
                        if '$DATA(Buren(tileID,todo(i),edge)) {
                            k transformations(trans)
                        }
                        s trans=$ORDER(transformations(trans))
                    }
                }
            }
            m Tiles(tileID)=transformations($ORDER(transformations("")))
            //zw transformations
            //return ""
            s x=$ORDER(Solution(y,x),1,tileID)
        }
        s y=$ORDER(Solution(y))
    }
}

ClassMethod GetEdge(Tiles, TileID, Place)
{
    // 1 => rechterbuur
    // 2 => linkerbuur
    // 3 => bovenbuur
    // 4 => onderbuur
    s edge=""
    if Place=1 {
        s x=10
        f y=1:1:10 {
            s edge=edge_$GET(Tiles(TileID,y,x))
        }
    }
    if Place=2 {
        s x=1
        f y=1:1:10 {
            s edge=edge_$GET(Tiles(TileID,y,x))
        }
    }
    if Place=3 {
        s y=1
        f x=1:1:10 {
            s edge=edge_$GET(Tiles(TileID,y,x))
        }
    }
    if Place=4 {
        s y=10
        f x=1:1:10 {
            s edge=edge_$GET(Tiles(TileID,y,x))
        }
    }
    return edge
}

ClassMethod CreateImage(Tiles, Solution, Image, RasterSize)
{
    s y=$ORDER(Solution(""))
    while y'="" {
        s startY=(y-1)*(RasterSize-2)+1
        //w !
        s x=$ORDER(Solution(y,""),1,tileID)
        while x'="" {
            s startX=(x-1)*(RasterSize-2)+1
            //w tileID," "
            for newY=2:1:9 {
                for newX=2:1:9 {
                    s Image(startY+newY-2,startX+newX-2)=Tiles(tileID,newY,newX)
                }
            } 
            s x=$ORDER(Solution(y,x),1,tileID)
        }
        s y=$ORDER(Solution(y))
    }
}

ClassMethod FindSolution(Tiles, Buren, ReverseBuren, Solution, EdgeSize)
{
    // all possible tiles can be tile 1,1
    // the next tile can be all possible neighbours of previous tile
    // go on till the end
    // make sure the only possible options for special tiles are special tiles
    // corners can only be corners, edges can only be edges
    // profit????

    // start with one corner tile. go along the edge and fill in all possibilities
    // start 2nd row and so on.
    s startTile=$ORDER(ReverseBuren(2,""))
    s curX=1
    s curY=1
    while ((curX'=0) && (curY'=0)) {
        // get a possible tile by comparing neighbours left and above.
        // if no neighbours take first.
        s tile=""
        k toMatch
        s toMatch=0
        k possibleTiles
        k newPossibleTiles
        if $DATA(Solution(curY,curX-1)) {
            //w !,"found left neighbhour"
            s toMatch=toMatch+1
            s toMatch(Solution(curY,curX-1))=""
            m possibleTiles=Buren(Solution(curY,curX-1))
        }
        if $DATA(Solution(curY-1,curX)) {
            //w !,"found top neighbhour"
            s toMatch=toMatch+1
            s toMatch(Solution(curY-1,curX))=""
            // create intersect met previous match, only if it exists
            if toMatch=2 {
                s buur=$ORDER(Buren(Solution(curY-1,curX),""))
                while buur'="" {
                    if $DATA(possibleTiles(buur)) {
                        s newPossibleTiles(buur)=""
                    }
                    s buur=$ORDER(Buren(Solution(curY-1,curX),buur))
                }
                k possibleTiles
                m possibleTiles=newPossibleTiles
            } else {
                m possibleTiles=Buren(Solution(curY-1,curX))
            }
        }
        if toMatch=0 {
            s tile=startTile
        } else {
            
            s neededBuren=4
            // find the first option that matches && that has the correct amount of buren
            if ..IsCorner(curX,curY,EdgeSize) {
                // neem de eerste optie met 2 buren die nog niet gekozen is
                s neededBuren=2
            } elseif ..IsEdge(curX,curY,EdgeSize) {
                // neem de eerste optie met 3 buren die nog niet gekozen is
                s neededBuren=3
            }
            //w !,"options:",*9,neededBuren,*9,toMatch,!
            //zw possibleTiles
            s buur=$ORDER(possibleTiles(""))
            while ((buur'="") && (tile="")) {
                if (Buren(buur)=neededBuren) && ('$DATA(used(buur))) {
                    s tile=buur
                }
                s buur=$ORDER(possibleTiles(buur))
            }
        }
        
        //w !,"used: ",tile," for ",curY,*9,curX
        s Solution(curY,curX)=tile
        s used(tile)=""
        d ..GetNext(.curX,.curY,EdgeSize)
    }

    //zw solution
}

ClassMethod IsEdge(CurX, CurY, EdgeSize)
{
    s result=0
    if ((CurX=1) || (CurY=1)) {
        s result=1
    }
    if ((CurX=EdgeSize) || (CurY=EdgeSize)) {
        s result=1
    }
    return result
}

ClassMethod IsCorner(CurX, CurY, EdgeSize)
{
    s result=0
    if ((CurX=EdgeSize) && ((CurY=1) || (CurY=EdgeSize))) {
        s result=1
    }
    if ((CurX=1) && ((CurY=1)||(CurY=EdgeSize))) {
        s result=1
    }
    return result
}

ClassMethod GetNext(CurX, CurY, EdgeSize)
{
    if CurX<EdgeSize {
        s CurX=CurX+1
    } elseif CurX=EdgeSize {
        s CurX=1
        s CurY=CurY+1
    }
    if CurY>EdgeSize {
        s CurX=0
        S CurY=0
    }
}

ClassMethod DrawTile(Tile)
{
    s y=$ORDER(Tile(""))
    while y'="" {
        w !
        s x=$ORDER(Tile(y,""),1,data)
        while x'="" {
            w data
            s x=$ORDER(Tile(y,x),1,data)
        }
        s y=$ORDER(Tile(y))
    }
}

ClassMethod GetPossibleBuren(Edges, TileID, Buren)
{
    k Buren
    s Buren=0
    // Kan waarschijnlijk efficienter door ze allemaal samen te doen, maar laten we later zien of dat nodig is
    s edge=$ORDER(Edges(TileID,"")) 
    while edge'="" {
        s tileID=$ORDER(Edges(""))
        while tileID'="" {
            if tileID'=TileID {
                if $DATA(Edges(tileID,edge)) {
                    if '$DATA(Buren(tileID)) {
                        s Buren=Buren+1
                    }
                    s Buren(tileID,edge)=""
                    
                }
            }
            s tileID=$ORDER(Edges(tileID))
        }

        s edge=$ORDER(Edges(TileID,edge)) 
    }
}

ClassMethod GetEdges(Tiles, TileID, Edges)
{
    k Edges
    s edge=""
    s edge2=""
    s edge3=""
    s edge4=""
    f i=1:1:10 {
        s edge=edge_Tiles(TileID,i,1)
        s edge2=edge2_Tiles(TileID,i,10)
        s edge3=edge3_Tiles(TileID,1,i)
        s edge4=edge4_Tiles(TileID,10,i)
    }
    s Edges(edge)=""
    s Edges(edge2)=""
    s Edges(edge3)=""
    s Edges(edge4)=""
    s Edges($REVERSE(edge))=""
    s Edges($REVERSE(edge2))=""
    s Edges($REVERSE(edge3))=""
    s Edges($REVERSE(edge4))=""
}

/* do we need this shit? */
ClassMethod Rotate(Tile)
{
    // transpose
    d ..Transpose(.Tile)
    // flip rows
    d ..FlipRows(.Tile)
}

ClassMethod FlipRows(Tile)
{
    s size=$ORDER(Tile(""),-1)
    for y=1:1:size {
        for x=1:1:size/2 {
            s temp=Tile(y,x)
            s Tile(y,x)=Tile(y,size-x+1)
            s Tile(y,size-x+1)=temp
        }
    }
}

ClassMethod Transpose(Tile)
{
    s size=$ORDER(Tile(""),-1)
    for y=1:1:size {
        for x=y:1:size {
            s temp=Tile(y,x)
            s Tile(y,x)=Tile(x,y)
            s Tile(x,y)=temp
        }
    }
}

}
