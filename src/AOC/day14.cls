Class AOC.day14
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day14.txt"
    if Example=1 s file="/aoc/day14example.txt"
    if Example=2 s file="/aoc/day14example2.txt"
    Set sc=stream.LinkToFile(file)

    While 'stream.AtEnd {
        s line=stream.ReadLine()
        s command=$PIECE(line," ")
        s argument=$PIECE(line," ",3)
        //w !,command,*9,argument
        if command="mask" {
            s mask=argument
        } else {
            s adr=$PIECE($PIECE(command,"]"),"[",2)
            k adrOptions
            s adrOptions=1
            s adrOptions(1)=$FACTOR(adr)
            //w *9,adr
            s bit=$FACTOR(argument)
            //w !,..BitstringToInt(bit),!
            for i=1:1:$LENGTH(mask) {
                s char=$EXTRACT(mask,*-i+1)
                if ((char=0) || (char=1)) {
                    s $BIT(bit,i)=char
                }
                // do part 2 here
                if char=0 {
                    // nothing
                } elseif char=1 {
                    // set 1 in all options
                    s opt=$ORDER(adrOptions(""),1,value)
                    while opt'="" {
                        s $BIT(value,i)=char
                        s adrOptions(opt)=value
                        s opt=$ORDER(adrOptions(opt),1,value)
                    }
                } elseif char="X" {
                    // create extra options for all possible combinations
                    k newOptions
                    s newOptions=0
                    s opt=$ORDER(adrOptions(""),1,value)
                    while opt'="" {
                        s newOptions=newOptions+1
                        s newValue=value
                        s $BIT(newValue,i)=1
                        s newOptions(newOptions)=newValue
                        s newOptions=newOptions+1
                        s newValue=value
                        s $BIT(newValue,i)=0
                        s newOptions(newOptions)=newValue
                        s opt=$ORDER(adrOptions(opt),1,value)
                    }
                    k adrOptions
                    m adrOptions=newOptions                   
                }
            }
            //w !,..BitstringToInt(bit),!
            s mem(adr,"BIT")=bit
            s mem(adr)=..BitstringToInt(bit)

            // do mem part 2 here?
            s opt=$ORDER(adrOptions(""),1,value)
            while opt'="" {
                s mem2(..BitstringToInt(value))=argument
                s opt=$ORDER(adrOptions(opt),1,value)
            }
        }
    }
    //zw mem
    s sum=0
    s adr=$ORDER(mem(""),1,value)
    while adr'="" {
        s sum=sum+value
        s adr=$ORDER(mem(adr),1,value)
    }
    w !,sum,!

    //zw mem2
    s sum=0
    s adr=$ORDER(mem2(""),1,value)
    while adr'="" {
        s sum=sum+value
        s adr=$ORDER(mem2(adr),1,value)
    }
    w !,sum,!
}

ClassMethod BitstringToInt(bitstring As %String)
{
   set bitint = 0
   set bit = 0 
   for {
      set bit = $bitfind(bitstring, 1, bit) 
      quit:'bit  
      set bitint = bitint + (2**bit) 
      set bit = bit + 1 
   }
   return bitint/2
}

}
