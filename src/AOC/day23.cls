Class AOC.day23
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day23.txt"
    if Example=1 s file="/aoc/day23example.txt"
    if Example=2 s file="/aoc/day23example2.txt"
    Set sc=stream.LinkToFile(file)

    While 'stream.AtEnd {
        s line=stream.ReadLine()
        s cups=line
    }

    /*
    s newstring="123456789"
    s nextThree="bert"
    s pos=1
    s cups=$EXTRACT(newstring,1,pos)_nextThree_$EXTRACT(newstring,pos+1,*)
    w !,cups
    s pos=9
    s cups=$EXTRACT(newstring,1,pos)_nextThree_$EXTRACT(newstring,pos+1,*)
    w !,cups
    s pos=8
    s cups=$EXTRACT(newstring,1,pos)_nextThree_$EXTRACT(newstring,pos+1,*)
    w !,cups
    */

    s rounds=100
    s curPos=1
    w !,cups
    w !,"amount of rounds: ",rounds
    f r=1:1:rounds {
        s char=$EXTRACT(cups,curPos)
        s origChar=char
        //w !,"-- round start: ",r," --"
        //w !,cups
        //w !,char
        // we kunnen gewoon de volgende 3 nemen
        s nextThree=$EXTRACT(cups,curPos+1,curPos+3)
        if $LENGTH(nextThree)<3 {
            s nextThree=nextThree_$EXTRACT(cups,1,3-$LENGTH(nextThree))
        }
        //w !,"pickup: ",nextThree
        s newstring=""
        for i=1:1:$LENGTH(cups) {
            s newchar=$EXTRACT(cups,i)
            if nextThree'[newchar {
                s newstring=newstring_newchar
            }
        }
        s isFound=0
        s pos=""
        while 'isFound {
            s char=char-1
            if char=0 s char=9
            // hier regel implementeren van char loop
            for i=1:1:$LENGTH(newstring) {
                if $EXTRACT(newstring,i)=char {
                    s pos=i
                    s isFound=1
                }
            }
        }
        //w !,"pos: ",pos,*9,char
        s cups=$EXTRACT(newstring,1,pos)_nextThree_$EXTRACT(newstring,pos+1,*)
        // find position of origChar that +1 is curPos
        s curPos=$LENGTH($PIECE(cups,origChar))+2
        if curPos>9 s curPos=1
    }
    w !,cups
    w !,$PIECE(cups,1,2)_$PIECE(cups,1)
}

ClassMethod Run2(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day23.txt"
    if Example=1 s file="/aoc/day23example.txt"
    if Example=2 s file="/aoc/day23example2.txt"
    Set sc=stream.LinkToFile(file)

    While 'stream.AtEnd {
        s line=stream.ReadLine()
        s cups=line
        for i=1:1:$LENGTH(line) {
            s content(i)=$EXTRACT(line,i)
            s content(i,"next")=i+1
            if $EXTRACT(line,i)=1 {
                s endCup=i
            }
            s reverse($EXTRACT(line,i))=i
        }
    }
    s maxValue=9
    
    for i=i+1:1:1000000 {
        s content(i)=i
        s content(i,"next")=i+1
        s reverse(i)=i
    }
    s maxValue=1000000
    
    s content($ORDER(content(""),-1),"next")=1
    s rounds=10000000
    s start=1
    for round=1:1:rounds {
        if '(round#100000) {
            w !,"---- starting round: ",round," ----",!
        }
        //d ..Print(.content)
        //zw content
        s cupValue=content(start)
        // cut out next 3 by finding the next number of 3rd and putting it in curPos next
        s next1=content(start,"next")
        s next2=content(next1,"next")
        s next3=content(next2,"next")
        s content(start,"next")=content(next3,"next") // 3 have been removed from 
        //w !,next1,*9,next2,*9,next3,*9,content(start,"next")
        //the list
        //d ..Print(.content)

        // find the cup with label current cup - 1
        s searchValue=cupValue-1
        if searchValue=0 s searchValue=maxValue
        // check if it is is in one of the next 3, if so do -1
        while ((searchValue=content(next1)) || (searchValue=content(next2)) || (searchValue=content(next3))) {
            s searchValue=searchValue-1
            if searchValue=0 s searchValue=maxValue
        }
        s curPos=reverse(searchValue)
    
        // curPos contains the searched value
        // insert the 3
        //w !,"found it at possition: ",curPos
        s last=content(curPos,"next")
        s content(curPos,"next")=next1
        s content(next3,"next")=last
        s start=content(start,"next")
    }
    //d ..Print(.content)
    //zw content

    s next1=content(endCup,"next")
    s next2=content(next1,"next")
    w !,content(next1),*9,content(next2),*9,content(next1)*content(next2)
}

ClassMethod Print(Content)
{
    s start=1
    w !,Content(start)
    s pos=Content(start,"next")
    while pos'=start {
        w Content(pos)
        s pos=Content(pos,"next")
    }
}

}
