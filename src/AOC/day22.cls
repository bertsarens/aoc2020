Class AOC.day22
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day22.txt"
    if Example=1 s file="/aoc/day22example.txt"
    if Example=2 s file="/aoc/day22example2.txt"
    Set sc=stream.LinkToFile(file)

    s player=1
    s $LI(player1,1)=0
    s $LI(player2,1)=0
    While 'stream.AtEnd {
        s line=stream.ReadLine()
        //w !,line
        if line["Player" {
            s player=$TR($PIECE(line," ",2),":")
            s pos=1
        } elseif line'="" {
            s pos=pos+1
            if player=1 {
                s $LI(player1,1)=$LI(player1,1)+1
                s $LI(player1,pos)=line
            } else {
                s $LI(player2,1)=$LI(player2,1)+1
                s $LI(player2,pos)=line
            }
        }
    }
    s result=..PlayGame(.player1,.player2)
    
    w !,$LTS(player1)
    w !,$LTS(player2)
    /*
    w !,$LTS(..MoveForward(player1))
    */
    //calc score
    if result=1 {
        w !,..CalcScore(player1)
    } else {
        w !,..CalcScore(player2)
    }
}

ClassMethod CalcScore(List)
{
    s sum=0
    s value=1
    for i=$LI(List,1)+1:-1:2 {
        s sum=sum+(value*$LI(List,i))
        s value=value+1
    }
    return sum
}

ClassMethod PlayGame(Player1, Player2)
{
    s winner=""
    s isDone=0
    s ronde=0
    while 'isDone {
        s ronde=ronde+1
        //w !,!,"ronde: ",ronde
        s addToHistory=$LTS(Player1)_$LTS(Player2)
        s card1=$LI(Player1,2)
        s card2=$LI(Player2,2)
        if $DATA(history(addToHistory)) {
            return 1
        } elseif (card1<=($LI(Player1,1)-1)) && 
                    (card2<=($LI(Player2,1)-1)) {
            // need subgame
            //w !,"starting subgame"
            s $LI(newPlayer1,1)=0
            for i=1:1:card1 {
                s $LI(newPlayer1,i+1)=$LI(Player1,i+2)
                s $LI(newPlayer1,1)=$LI(newPlayer1,1)+1
            }
            s $LI(newPlayer2,1)=0
            for i=1:1:card2 {
                s $LI(newPlayer2,i+1)=$LI(Player2,i+2)
                s $LI(newPlayer2,1)=$LI(newPlayer2,1)+1
            }
            s wins=..PlayGame(newPlayer1,newPlayer2)
        } elseif card1>card2 {
            s wins=1
        } else {
            s wins=2
        }
        s history(addToHistory)=""
        if wins=1 {
            s $LI(Player1,$LI(Player1,1)+2)=$LI(Player1,2)
            s $LI(Player1,$LI(Player1,1)+3)=$LI(Player2,2)
            s $LI(Player1,1)=$LI(Player1,1)+1
            s $LI(Player2,1)=$LI(Player2,1)-1
        } else {
            s $LI(Player2,$LI(Player2,1)+2)=$LI(Player2,2)
            s $LI(Player2,$LI(Player2,1)+3)=$LI(Player1,2)
            s $LI(Player2,1)=$LI(Player2,1)+1
            s $LI(Player1,1)=$LI(Player1,1)-1
        }
        //w !,$LTS(Player1)
        //w !,$LTS(Player2)

        s Player1=..MoveForward(Player1)
        s Player2=..MoveForward(Player2)
        
        //w !,$LTS(Player1)
        //w !,$LTS(Player2)
        if ($LI(Player1,1)=0) {
            s isDone=1
            s winner=2
        }
        if ($LI(Player2,1)=0) {
            s isDone=1
            s winner=1
        }
        
    }

    return winner
}

ClassMethod MoveForward(List)
{
    s $LI(return,1)=$LI(List,1)
    for i=2:1:$LI(return,1)+1 {
        s $LI(return,i)=$LG(List,i+1)
    }
    return return
}

}
