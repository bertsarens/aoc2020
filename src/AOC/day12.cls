Class AOC.day12
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day12.txt"
    if Example=1 s file="/aoc/day12example.txt"
    Set sc=stream.LinkToFile(file)

    s curX=0
    s curY=0
    s curX2=0
    s curY2=0
    s wayX=10
    s wayY=1

    s dir(1,"dx")=1
    s dir(1,"dy")=0
    s dir(2,"dx")=0
    s dir(2,"dy")=-1
    s dir(3,"dx")=-1
    s dir(3,"dy")=0
    s dir(4,"dx")=0
    s dir(4,"dy")=1

    s curDir=1

    While 'stream.AtEnd {
        s line=stream.ReadLine()
        s action=$EXTRACT(line)
        s number=$EXTRACT(line,2,*)
        //w !,action,*9,number
        s useDir=""
        if action="N" {
            s useDir=4
            s wayY=wayY+number
        }
        if action="S" {
            s useDir=2
            s wayY=wayY-number
        }
        if action="E" {
            s useDir=1
            s wayX=wayX+number
        }
        if action="W" {
            s useDir=3
            s wayX=wayX-number
        }
        if action="F" {
            s useDir=curDir
            s curX2=curX2+(wayX*number)
            s curY2=curY2+(wayY*number)
        }
        if (action="R") || (action="L") {
            s spin=-1
             if (action="R") {
                s spin=1
             }
             s todo=number\90
             s newDir=curDir
             //w !,"curDir = ",curDir
             // w !,"todo = ",todo
             f i=1:1:todo {
                 s newDir=$ORDER(dir(newDir),spin)
                 if newDir="" s newDir=$ORDER(dir(newDir),spin)
                 if (spin=1) {
                     s prevX=wayX
                     s wayX=wayY
                     s wayY=-prevX
                 } else {
                     s prevX=wayX
                     s wayX=-wayY
                     s wayY=prevX                    
                 }
             }
             //w !,"newDir = ",newDir
             s curDir=newDir
        }
        if useDir'="" {
            s curX=curX+(dir(useDir,"dx")*number)
            s curY=curY+(dir(useDir,"dy")*number)
        }
        //w !,curX,*9,curY
        //w !,action,*9,number
        //w !,wayX,*9,wayY
        //w !,curX2,*9,curY2
    }
    w !,"result",*9,$ZABS(curY)+$ZABS(curX)
    w !,"result2",*9,$ZABS(curY2)+$ZABS(curX2)
}

}
