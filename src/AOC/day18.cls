Class AOC.day18
{

ClassMethod Run(Example As %Boolean) [ PublicList = x ]
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day18.txt"
    if Example=1 s file="/aoc/day18example.txt"
    Set sc=stream.LinkToFile(file)

    s sum=0
    s lines=0
    s sum2=0
    While 'stream.AtEnd {
        s line=stream.ReadLine()
        s com="s x="_line
        x com
        s sum=sum+x
        s lines=lines+1
        s lines(lines)=line
        s result=..Calculate(line,1,1)
        s sum2=sum2+result
    }
    w !,sum
    w !,sum2
    // loop er door
    // houdt alles bij
    // als je haakje tegenkomt ga niveau dieper ( recursie)
    // als je een sluit haakje tegenkomt bereken en ga niveau omhoog
}

ClassMethod Calculate(Line, Pos, EndPos)
{
    s toCalc=""
    for i=Pos:1:$LENGTH(Line) {
        s EndPos=i
        s char=$EXTRACT(Line,i)
        //w !,"char: ",*9,char
        if char="(" {
            s char=..Calculate(Line,i+1,.EndPos)
            s i=EndPos
        }
        if char=")" {
            //w !,toCalc
            s EndPos=i
            return ..Solve(toCalc)
        }
        s toCalc=toCalc_char
    }
    //w !,toCalc
    return ..Solve(toCalc)
}

ClassMethod Solve(Line) [ PublicList = y ]
{
    s result=0
    // "3 * 3 + 2 * 4"
    s newline=""
    for i=1:1:$LENGTH(Line,"*") {
            
        s piece=$PIECE(Line,"*",i)
        s todo="s y="_piece
        x todo
        if newline="" {
            s newline=newline_y
        } else {
            s newline=newline_" * "_y
        }
    }
    s todo="s y="_newline
    x todo
    s result=y
    return result
}

}
