Class AOC.day15
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day15.txt"
    if Example=1 s file="/aoc/day15example.txt"
    if Example=2 s file="/aoc/day15example2.txt"
    Set sc=stream.LinkToFile(file)

    s turn=1
    s lastNumber=""
    While 'stream.AtEnd {
        s line=stream.ReadLine()
        //w !,line
       
        for i=1:1:$LENGTH(line,",") {
            if lastNumber'="" {
                s numbers(lastNumber)=i-1
            }
            s turn=turn+1
            s lastNumber=$PIECE(line,",",i)
        }
    }
    for i=turn:1:30000000 {
        if $DATA(numbers(lastNumber)) {
            s new=i-1-numbers(lastNumber)
        } else {
            s new=0
        }
        s numbers(lastNumber)=i-1
        //w !,i,*9,lastNumber,*9,new
        s lastNumber=new
    }
    w !,lastNumber
    //zw
}

}
