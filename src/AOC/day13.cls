Class AOC.day13
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day13.txt"
    s startT=100000000000000
    if Example=1 {
        s file="/aoc/day13example.txt"
        s startT=0
    }
    Set sc=stream.LinkToFile(file)

    s goal=stream.ReadLine()
    s minNumber=goal*goal
    s busses=$LFS(stream.ReadLine(),",")
    s offset=0
    for i=1:1:$LL(busses) {
        s current=0
        s busID=$LI(busses,i)
        if busID'="x" {
            while current<goal {
                s current=current+busID
            }
            if current<minNumber {
                s minNumber=current
                s minBusID=busID
            }
            s newBusses(busID)=offset
        }
        s offset=offset+1
       
    }
    w !,minBusID,*9,minNumber
    w !,"result:",*9,(minNumber-goal)*minBusID

    /*
    s isDone=0
    s t=startT+1
    while 'isDone {
        s dt=0
        s notFound=0
        s busID=$ORDER(newBusses(""),-1)
        while busID'="" {
            if 'notFound {
                if ((t+dt)#busID) {
                        s notFound=1
                    }
                s dt=dt+1
            }
            s busID=$ORDER(newBusses(busID),-1)
        }
        s isDone=1
        s t=t+1      
    }
    w !,"result2",*9,t
    */

    d ..newAttempt(.newBusses)
}

ClassMethod newAttempt(Busses)
{
    s t=1
    s jump=1
    s busID=$ORDER(Busses(""),-1,offset)
    while busID'="" {
        while (t+offset)#busID {
            s t=t+jump
        }
        s jump=jump*busID
        s busID=$ORDER(Busses(busID),-1,offset)
    }    
    
    w !,"result2",*9,t
}

}
