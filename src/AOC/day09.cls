Class AOC.day09
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day09.txt"
    if Example s file="/aoc/day09example.txt"
    Set sc=stream.LinkToFile(file)

    s instructions=0

    s id=1
    While 'stream.AtEnd {
        s line=stream.ReadLine()
        s numbers(id)=line
        s id=id+1
    }
    //zw numbers
    s pre=25
    if Example s pre=5
    s isDone=0

    s next=$ORDER(numbers(pre),1,number)
    while (next'="") && ('isDone) {
        s isValid=..IsValid(.numbers,number,next-pre,next-1)
        if 'isValid {
            s result=number
        }
        s next=$ORDER(numbers(next),1,number)
    }
    w !,result

    d ..FindSet(.numbers,result)
}

ClassMethod FindSet(Numbers, Number)
{
    s isFound=0
    s start=$ORDER(Numbers(""))
    while ((start'="") && ('isFound)) {
        s sum=Numbers(start)
        s end=start
        while sum<Number {
            s end=end+1
            s sum=sum+Numbers(end)
        }
        if sum=Number {
            w !,start,*9,end,*9,Numbers(start),*9,Numbers(end),*9,sum
            s min=Numbers(start)
            s max=Numbers(start)
            f i=start:1:end {
                if Numbers(i)<min s min=Numbers(i)
                if Numbers(i)>max s max=Numbers(i)
            }
            w !,"result",*9,min,*9,max,*9,min+max
            s isFound=1
        }
        s start=$ORDER(Numbers(start))
    }
}

ClassMethod IsValid(Numbers, Number, Min, Max)
{
    //w !,Number,*9,Min,*9,Max
    s result =0
    // make this while loops
    for i=Min:1:Max {
        for j=Min+1:1:Max {
            if ((Numbers(i)'=Numbers(j)) && ((Numbers(i)+Numbers(j))=Number)) {
                s result=1
                //w !,Numbers(i),*9,Numbers(j),*9,Number
            }
        }
    }
    

    return result
}

}
