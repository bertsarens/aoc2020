Class AOC.day04
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day04.txt"
    if Example s file="/aoc/day04example.txt"
    Set sc=stream.LinkToFile(file)
    s count=0


    //s valid("cid")=""

    s result=0
    While 'stream.AtEnd {
        s line=stream.ReadLine()
        if line="" {
            k passport
            m passport=passports(count)
            if ..ValidPassport(.passport) s result=result+1
            s count=count+1
        } else {
            for i=1:1:$LENGTH(line," ") {
                s passports(count,$PIECE($PIECE(line," ",i),":"))=$PIECE($PIECE(line," ",i),":",2)
            }
        }

    }
    k passport
    m passport=passports(count)
    if ..ValidPassport(.passport) s result=result+1
    w !,result
}

ClassMethod validEcl(Value)
{
    if Value="" return 0
    s valid("amb")=""
    s valid("blu")=""
    s valid("brn")=""
    s valid("gry")=""
    s valid("grn")=""
    s valid("hzl")=""
    s valid("oth")=""

    return $DATA(valid(Value))
}

ClassMethod validNumber(Value, Min, Max, Length)
{
    s result=1
    if ($LENGTH(Value)'=Length) {
        s result=0
    }
    if '$NUMBER(Value) {
        s result=0
    } else {
        if (Value<Min) || (Value>Max) {
            s result=0
        }
    }
    // w !,Value,*9,Min,*9,Max,*9,result
    return result
}

ClassMethod ValidColor(Value)
{
    if (($EXTRACT(Value)'="#") || ($LENGTH(Value)'=7)) {
        return 0
    }
    for i=0:1:9 {
        s valid(i)=1
    }
    for s="a","b","c","d","e","f" {
        s valid(s)=""
    }
    s result=1
    f i=1:1:$LENGTH($EXTRACT(Value,2,7)) {
        if '$DATA(valid($EXTRACT($EXTRACT(Value,2,7),i))) {
            s result=0
        }
    }
    w !,Value,*9,result
    return result
}

ClassMethod ValidHGT(Value)
{
    s invalid=0
    if ((Value'["in") && (Value'["cm")) {
        s invalid = 1
    } elseif Value["in" {
        if '..validNumber(+Value,59,76,2) s invalid=1
    } elseif Value["cm" {
        if '..validNumber(+Value,150,193,3) s invalid=1
    }
    return 'invalid
}

ClassMethod ValidPID(Value)
{
    s result=1
    if $LENGTH(Value)'=9 {
        s result=0
    }
    for i=0:1:9 {
        s valid(i)=1
    }
    
    f i=1:1:$LENGTH(Value) {
        if '$DATA(valid($EXTRACT(Value,i))) {
            s result=0
        }
    }
    return result
}

ClassMethod ValidPassport(Passport)
{
    s valid("byr")=""
    s valid("iyr")=""
    s valid("eyr")=""
    s valid("hgt")=""
    s valid("hcl")=""
    s valid("ecl")=""
    s valid("pid")=""

    s invalid=0
    s type=$ORDER(valid(""))
    while type'="" {
        s value=""
        if $DATA(Passport(type)) {
            s value=Passport(type)
        }
        if '$DATA(Passport(type)) {
            s invalid=1
        } elseif type="byr" {
            //  @'fbyr (Birth Year) - four digits; at least 1920 and at most 2002.
            if ('..validNumber(value,1920,2002,4)) s invalid=1
        } elseif type="iyr" {
            //iyr (Issue Year) - four digits; at least 2010 and at most 2020.
            if ('..validNumber(value,2010,2020,4)) s invalid=1
        } elseif type="eyr" {
            //eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
            if ('..validNumber(value,2020,2030,4)) s invalid=1
        } elseif type="hgt" {
            //hgt (Height) - a number followed by either cm or in:
            //If cm, the number must be at least 150 and at most 193.
            //If in, the number must be at least 59 and at most 76.
            if '..ValidHGT(value) {
                s invalid = 1
            } 
        } elseif type="hcl" {
            //hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
            if '..ValidColor(value) {
                s invalid=1
            } 
        } elseif type="ecl" {
            //ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
            if '..validEcl(value) {
                s invalid=1
            }
        } elseif type="pid" {
            //pid (Passport ID) - a nine-digit number, including leading zeroes.
            if ('..ValidPID(value)) {
                s invalid=1
            }
        }
        s type=$ORDER(valid(type))
    }

    return 'invalid
}

}
