Class AOC.day10
{

ClassMethod Run(Example As %Boolean)
{
    Set stream=##class(%Stream.FileCharacter).%New()
    s file="/aoc/day10.txt"
    if Example=1 s file="/aoc/day10example.txt"
    if Example=2 s file="/aoc/day10example2.txt"
    Set sc=stream.LinkToFile(file)

    s instructions=0

    While 'stream.AtEnd {
        s line=stream.ReadLine()
        s numbers(line)=""
    }
    s numbers($ORDER(numbers(""),-1)+3)=""

    s prevNumber=0
    s number=$ORDER(numbers(""))
    while number'="" {
        s diffs(number-prevNumber)=$GET(diffs(number-prevNumber))+1
        s prevNumber=number
        s number=$ORDER(numbers(number))
    }
    //s diffs(3)=diffs(3)+1
    w !,diffs(3)*diffs(1)

    s result=..GetAllOptions(.numbers,0,.history)
    w !,"end is: ",result
    //zw history
}

ClassMethod GetAllOptions(Numbers, CurrentNumber, History)
{
    //w !,"start"
    s result=0
    for i=1:1:3 {
        if $DATA(Numbers(CurrentNumber+i)) {
            //w !,CurrentNumber+i
            if '$DATA(History(CurrentNumber+i)) {
                //w " ",CurrentNumber+i
                s History(CurrentNumber+i)=..GetAllOptions(.Numbers,CurrentNumber+i,.History)
            } else {
                //w "found: ",CurrentNumber+i
            }
            s result=result+History(CurrentNumber+i)
            //w !,"result",*9,result," + history",*9,History(CurrentNumber+i)
            if ((CurrentNumber+i)=$ORDER(Numbers(""),-1)) {
                s result=result+1
            }
            
        }
    }
    return result
}

}
